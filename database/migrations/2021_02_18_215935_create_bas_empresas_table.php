<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBasEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bas_empresas', function (Blueprint $table) {
            $table->id();
            $table->string("nombre_corto", 50);
            $table->string("empresa", 150);
            $table->string("mail", 50);
            $table->string("tel_fijo", 15);
            $table->string("tel_movil", 15);
            $table->boolean("estatus")->default(true);
            $table->unsignedBigInteger('id_bas_direccion');
            $table->foreign('id_bas_direccion')->references('id')->on('bas_direccions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bas_empresas');
    }
}
