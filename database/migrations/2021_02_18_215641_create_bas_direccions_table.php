<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBasDireccionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bas_direccions', function (Blueprint $table) {
            $table->id();
            $table->string("calle", 100);
            $table->string("num_ext", 5);
            $table->string("num_int", 5);
            $table->string("colonia", 100);
            $table->string("cp", 6);
            $table->string("municipio", 20);
            $table->string("estado", 20);
            $table->text("referencia");
            $table->boolean("estatus")->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bas_direccions');
    }
}
