<?php

namespace App\Http\Controllers;

use App\InvInventario;
use Illuminate\Http\Request;

class InvInventarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InvInventario  $invInventario
     * @return \Illuminate\Http\Response
     */
    public function show(InvInventario $invInventario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InvInventario  $invInventario
     * @return \Illuminate\Http\Response
     */
    public function edit(InvInventario $invInventario)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InvInventario  $invInventario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InvInventario $invInventario)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InvInventario  $invInventario
     * @return \Illuminate\Http\Response
     */
    public function destroy(InvInventario $invInventario)
    {
        //
    }
}
