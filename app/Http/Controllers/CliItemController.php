<?php

namespace App\Http\Controllers;

use App\CliItem;
use Illuminate\Http\Request;

class CliItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CliItem  $cliItem
     * @return \Illuminate\Http\Response
     */
    public function show(CliItem $cliItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CliItem  $cliItem
     * @return \Illuminate\Http\Response
     */
    public function edit(CliItem $cliItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CliItem  $cliItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CliItem $cliItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CliItem  $cliItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(CliItem $cliItem)
    {
        //
    }
}
