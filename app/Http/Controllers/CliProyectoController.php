<?php

namespace App\Http\Controllers;

use App\CliProyecto;
use Illuminate\Http\Request;

class CliProyectoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CliProyecto  $cliProyecto
     * @return \Illuminate\Http\Response
     */
    public function show(CliProyecto $cliProyecto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CliProyecto  $cliProyecto
     * @return \Illuminate\Http\Response
     */
    public function edit(CliProyecto $cliProyecto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CliProyecto  $cliProyecto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CliProyecto $cliProyecto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CliProyecto  $cliProyecto
     * @return \Illuminate\Http\Response
     */
    public function destroy(CliProyecto $cliProyecto)
    {
        //
    }
}
