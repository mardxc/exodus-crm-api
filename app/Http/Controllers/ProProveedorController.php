<?php

namespace App\Http\Controllers;

use App\ProProveedor;
use Illuminate\Http\Request;

class ProProveedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProProveedor  $proProveedor
     * @return \Illuminate\Http\Response
     */
    public function show(ProProveedor $proProveedor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProProveedor  $proProveedor
     * @return \Illuminate\Http\Response
     */
    public function edit(ProProveedor $proProveedor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProProveedor  $proProveedor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProProveedor $proProveedor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProProveedor  $proProveedor
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProProveedor $proProveedor)
    {
        //
    }
}
