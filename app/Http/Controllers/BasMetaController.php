<?php

namespace App\Http\Controllers;

use App\BasMeta;
use Illuminate\Http\Request;

class BasMetaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BasMeta  $basMeta
     * @return \Illuminate\Http\Response
     */
    public function show(BasMeta $basMeta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BasMeta  $basMeta
     * @return \Illuminate\Http\Response
     */
    public function edit(BasMeta $basMeta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BasMeta  $basMeta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BasMeta $basMeta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BasMeta  $basMeta
     * @return \Illuminate\Http\Response
     */
    public function destroy(BasMeta $basMeta)
    {
        //
    }
}
