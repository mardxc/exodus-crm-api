<?php

namespace App\Http\Controllers;

use App\CliCliente;
use Illuminate\Http\Request;

class CliClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CliCliente  $cliCliente
     * @return \Illuminate\Http\Response
     */
    public function show(CliCliente $cliCliente)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CliCliente  $cliCliente
     * @return \Illuminate\Http\Response
     */
    public function edit(CliCliente $cliCliente)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CliCliente  $cliCliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CliCliente $cliCliente)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CliCliente  $cliCliente
     * @return \Illuminate\Http\Response
     */
    public function destroy(CliCliente $cliCliente)
    {
        //
    }
}
