<?php

namespace App\Http\Controllers;

use App\BasDireccion;
use Illuminate\Http\Request;

class BasDireccionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BasDireccion  $basDireccion
     * @return \Illuminate\Http\Response
     */
    public function show(BasDireccion $basDireccion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BasDireccion  $basDireccion
     * @return \Illuminate\Http\Response
     */
    public function edit(BasDireccion $basDireccion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BasDireccion  $basDireccion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BasDireccion $basDireccion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BasDireccion  $basDireccion
     * @return \Illuminate\Http\Response
     */
    public function destroy(BasDireccion $basDireccion)
    {
        //
    }
}
