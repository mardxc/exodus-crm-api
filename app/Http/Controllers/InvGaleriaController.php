<?php

namespace App\Http\Controllers;

use App\InvGaleria;
use Illuminate\Http\Request;

class InvGaleriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InvGaleria  $invGaleria
     * @return \Illuminate\Http\Response
     */
    public function show(InvGaleria $invGaleria)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InvGaleria  $invGaleria
     * @return \Illuminate\Http\Response
     */
    public function edit(InvGaleria $invGaleria)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InvGaleria  $invGaleria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InvGaleria $invGaleria)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InvGaleria  $invGaleria
     * @return \Illuminate\Http\Response
     */
    public function destroy(InvGaleria $invGaleria)
    {
        //
    }
}
