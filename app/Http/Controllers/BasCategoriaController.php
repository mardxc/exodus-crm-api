<?php

namespace App\Http\Controllers;

use App\BasCategoria;
use BasCategoriaSeeder;
use Illuminate\Http\Request;

class BasCategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return BasCategoria::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return BasCategoria::create( $request->all() );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BasCategoria  $basCategoria
     * @return \Illuminate\Http\Response
     */
    public function show(BasCategoria $basCategoria)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BasCategoria  $basCategoria
     * @return \Illuminate\Http\Response
     */
    public function edit(BasCategoria $basCategoria)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BasCategoria  $basCategoria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BasCategoria $basCategoria)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BasCategoria  $basCategoria
     * @return \Illuminate\Http\Response
     */
    public function destroy(BasCategoria $basCategoria)
    {
        //
    }
}
