<?php

namespace App\Http\Controllers;

use App\InvProducto;
use Illuminate\Http\Request;

class InvProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InvProducto  $invProducto
     * @return \Illuminate\Http\Response
     */
    public function show(InvProducto $invProducto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InvProducto  $invProducto
     * @return \Illuminate\Http\Response
     */
    public function edit(InvProducto $invProducto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InvProducto  $invProducto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InvProducto $invProducto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InvProducto  $invProducto
     * @return \Illuminate\Http\Response
     */
    public function destroy(InvProducto $invProducto)
    {
        //
    }
}
