<?php

namespace App\Http\Controllers;

use App\BasRegalo;
use Illuminate\Http\Request;

class BasRegaloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BasRegalo  $basRegalo
     * @return \Illuminate\Http\Response
     */
    public function show(BasRegalo $basRegalo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BasRegalo  $basRegalo
     * @return \Illuminate\Http\Response
     */
    public function edit(BasRegalo $basRegalo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BasRegalo  $basRegalo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BasRegalo $basRegalo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BasRegalo  $basRegalo
     * @return \Illuminate\Http\Response
     */
    public function destroy(BasRegalo $basRegalo)
    {
        //
    }
}
