<?php

namespace App\Http\Controllers;

use App\InvEntrada;
use Illuminate\Http\Request;

class InvEntradaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InvEntrada  $invEntrada
     * @return \Illuminate\Http\Response
     */
    public function show(InvEntrada $invEntrada)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InvEntrada  $invEntrada
     * @return \Illuminate\Http\Response
     */
    public function edit(InvEntrada $invEntrada)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InvEntrada  $invEntrada
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InvEntrada $invEntrada)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InvEntrada  $invEntrada
     * @return \Illuminate\Http\Response
     */
    public function destroy(InvEntrada $invEntrada)
    {
        //
    }
}
