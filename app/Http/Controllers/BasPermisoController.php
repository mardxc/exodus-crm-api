<?php

namespace App\Http\Controllers;

use App\BasPermiso;
use Illuminate\Http\Request;

class BasPermisoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BasPermiso  $basPermiso
     * @return \Illuminate\Http\Response
     */
    public function show(BasPermiso $basPermiso)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BasPermiso  $basPermiso
     * @return \Illuminate\Http\Response
     */
    public function edit(BasPermiso $basPermiso)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BasPermiso  $basPermiso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BasPermiso $basPermiso)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BasPermiso  $basPermiso
     * @return \Illuminate\Http\Response
     */
    public function destroy(BasPermiso $basPermiso)
    {
        //
    }
}
