<?php

namespace App\Http\Controllers;

use App\CliCotizacion;
use Illuminate\Http\Request;

class CliCotizacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CliCotizacion  $cliCotizacion
     * @return \Illuminate\Http\Response
     */
    public function show(CliCotizacion $cliCotizacion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CliCotizacion  $cliCotizacion
     * @return \Illuminate\Http\Response
     */
    public function edit(CliCotizacion $cliCotizacion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CliCotizacion  $cliCotizacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CliCotizacion $cliCotizacion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CliCotizacion  $cliCotizacion
     * @return \Illuminate\Http\Response
     */
    public function destroy(CliCotizacion $cliCotizacion)
    {
        //
    }
}
