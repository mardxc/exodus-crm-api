<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BasCategoria extends Model
{
    protected $fillable = ["descripcion", "estatus"];
}
